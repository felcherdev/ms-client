var request = require('request-promise');

module.exports = function () {
    return {
        read: (id = null, searchByToken = false) => {
            return request({
                url: this.domain + '/auth/users' + (id ? '/' + id : ''),
                method: 'POST',
                headers: this.getHeaders(),
                json: true,
                qs: {
                    searchByToken: searchByToken
                }
            });
        },
        create: (data) => {
            return request({
                url: this.domain + '/auth/users/new',
                method: 'POST',
                headers: this.getHeaders(),
                body: data,
                json: true
            });
        },
        update: (id, data) => {
            return request({
                url: this.domain + '/auth/users/' + id,
                method: 'PATCH',
                headers: this.getHeaders(),
                body: data,
                json: true
            });
        },
        delete: (
            id,
            deleteTransaction = false // If passed as 'true', the 'id' field will be the transactionId to delete from the users table
        ) => {
            return request({
                url: this.domain + '/auth/users/' + id,
                method: 'DELETE',
                headers: this.getHeaders(),
                json: true,
                qs: {
                    deleteTransaction: deleteTransaction
                }
            });
        }

    }
}