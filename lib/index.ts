var request = require('request-promise');
const verifyToken = require('./token').verifyToken;
var fs = require('fs');
declare const __:any;


class MSClient {
    token?: String;
    contract: any;
    domain: String;
    auth: String;
    users: any;
    mailer: any;
    licences: any;
    filestorage: any;
    id: any;    // Can either be a DLK or a domain name for the app

    getHeaders() {
        return {
            // 'Accept-Language': global.lang,  // Passo l'header della linga per l'internazionalizzazione
            'Accept-Language': 'en',  // Passo l'header della linga per l'internazionalizzazione
            'token': this.token,
            'origin': process.env.MS == 'true' ? 'microservice' : 'gateway'
        };
    }
    
    setToken(token) {
        var decoded = verifyToken(token || this.token);
        this.contract = decoded;
        this.token = token;
        if(!global.isMS) console.log(__( 'Licenza esistente e in corso di validità.'));
        return decoded;
    }

    constructor(token) {
        
        // If the user is already authenticated, we just store the token in the class,
        // ready to be used in case of API call.
        if(token) this.setToken(token);

        this.domain = global.settings.msHost;
        this.auth = require('./authentication').bind(this)();
        this.users = require('./users').bind(this)();

        this.mailer = {
            send: (data) => {
                return request({
                    url: this.domain + '/mailer/api/send',
                    method: 'POST',
                    headers: this.getHeaders(),
                    body: data,
                    json: true
                });
            }
        }

        this.licences = {
            getByKeyOrId: (identifier) => {
                return request({
                    url: this.domain + '/licences/public/microservice/authenticate/' + identifier,
                    headers: this.getHeaders(),
                    resolveWithFullResponse: true,
                    json: true
                });
            }
        }

        this.filestorage = {
            upload: (file, mediaId) => {
                return new Promise((resolve, reject) => {
                    request({
                        url: this.domain + '/filestorage/api/media/upload',
                        method: 'POST',
                        headers: this.getHeaders(),
                        resolveWithFullResponse: true,
                        formData: {
                            file: fs.createReadStream(file.path)
                        },
                        qs: {
                            mediaId: mediaId
                        }
                    }).then(function(res){
                        res.body = JSON.parse(res.body)
                        resolve(res);
                    }).catch((err) => {
                        err.body = JSON.parse(err.message);
                        reject(err);
                    });
                });
            }
        }
        
        return this;
    }

    async authenticate(
        id  
    ) {
        return new Promise((resolve, reject) => {
            this.id = id || process.env.DMLICENCEKEY;
            if(!this.id) {
                reject(new Error(__( "Specificare una chiave di licenza nella variabile d'ambiente DMLICENCEKEY per utilizzare l'applicazione." )));
            }
            
            if(!global.isMS) console.log(__('Controllando la licenza %s ...', this.id));

            this.licences.getByKeyOrId(this.id)
            .then((response) => {
                var body = response.body;
                this.setToken(body.token);
                resolve(body.token);
            })
            .catch((err) => {
                console.log(err)
                reject(new Error('* ID: ' + this.id + ' * <-- Error while checking the licence: ' + err.response.body.errors))
            });
        });
    }
}

module.exports = MSClient;