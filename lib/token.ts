var jwt = require('jsonwebtoken');
var fs = require('fs');
import * as __ from 'i18n';

module.exports = {
    verifyToken(token) {
        // Leggo la chiave pubblica dalla cartella della libreria.
        var cert = fs.readFileSync(__dirname + '/../licence_cert.cer').toString('utf8');

        // Verifico il token grazie alla chiave pubblica in modo asimmetrico.
        try {
            var decoded = jwt.verify(token, cert, {
                algorithms: ['RS256']
            });
            return decoded;
        } catch (err) {
            throw new Error(__('Errore durante la verifica del token:') + ' ' + err.message);
        }
    }
}