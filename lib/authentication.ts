var request = require('request-promise');

module.exports = function () {
    return {
        login: (data) => {
            return request({
                url: this.domain + '/auth/public/login',
                method: 'POST',
                headers: this.getHeaders(),
                body: data,
                json: true
            });
        },
        resetPassword: (email) => {
            return request({
                url: this.domain + '/auth/public/password-reset/' + email,
                method: 'GET',
                headers: this.getHeaders(),
                json: true
            });
        },
        confirmResetPassword: (token, password) => {
            return request({
                url: this.domain + '/auth/public/password-reset/' + token,
                method: 'POST',
                headers: this.getHeaders(),
                json: true,
                body: {
                    password: password
                }
            });
        }
    }
}