declare var request: any;
declare const verifyToken: any;
declare var fs: any;
declare const __: any;
declare class MSClient {
    token?: String;
    contract: any;
    domain: String;
    auth: String;
    users: any;
    mailer: any;
    licences: any;
    filestorage: any;
    id: any;
    getHeaders(): {
        'Accept-Language': string;
        'token': String;
        'origin': string;
    };
    setToken(token: any): any;
    constructor(token: any);
    authenticate(id: any): Promise<unknown>;
}
