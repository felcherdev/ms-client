var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var request = require('request-promise');
var verifyToken = require('./token').verifyToken;
var fs = require('fs');
var MSClient = /** @class */ (function () {
    function MSClient(token) {
        var _this = this;
        // If the user is already authenticated, we just store the token in the class,
        // ready to be used in case of API call.
        if (token)
            this.setToken(token);
        this.domain = global.settings.msHost;
        this.auth = require('./authentication').bind(this)();
        this.users = require('./users').bind(this)();
        this.mailer = {
            send: function (data) {
                return request({
                    url: _this.domain + '/mailer/api/send',
                    method: 'POST',
                    headers: _this.getHeaders(),
                    body: data,
                    json: true
                });
            }
        };
        this.licences = {
            getByKeyOrId: function (identifier) {
                return request({
                    url: _this.domain + '/licences/public/microservice/authenticate/' + identifier,
                    headers: _this.getHeaders(),
                    resolveWithFullResponse: true,
                    json: true
                });
            }
        };
        this.filestorage = {
            upload: function (file, mediaId) {
                return new Promise(function (resolve, reject) {
                    request({
                        url: _this.domain + '/filestorage/api/media/upload',
                        method: 'POST',
                        headers: _this.getHeaders(),
                        resolveWithFullResponse: true,
                        formData: {
                            file: fs.createReadStream(file.path)
                        },
                        qs: {
                            mediaId: mediaId
                        }
                    }).then(function (res) {
                        res.body = JSON.parse(res.body);
                        resolve(res);
                    }).catch(function (err) {
                        err.body = JSON.parse(err.message);
                        reject(err);
                    });
                });
            }
        };
        return this;
    }
    MSClient.prototype.getHeaders = function () {
        return {
            // 'Accept-Language': global.lang,  // Passo l'header della linga per l'internazionalizzazione
            'Accept-Language': 'en',
            'token': this.token,
            'origin': process.env.MS == 'true' ? 'microservice' : 'gateway'
        };
    };
    MSClient.prototype.setToken = function (token) {
        var decoded = verifyToken(token || this.token);
        this.contract = decoded;
        this.token = token;
        if (!global.isMS)
            console.log(__('Licenza esistente e in corso di validità.'));
        return decoded;
    };
    MSClient.prototype.authenticate = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.id = id || process.env.DMLICENCEKEY;
                        if (!_this.id) {
                            reject(new Error(__("Specificare una chiave di licenza nella variabile d'ambiente DMLICENCEKEY per utilizzare l'applicazione.")));
                        }
                        if (!global.isMS)
                            console.log(__('Controllando la licenza %s ...', _this.id));
                        _this.licences.getByKeyOrId(_this.id)
                            .then(function (response) {
                            var body = response.body;
                            _this.setToken(body.token);
                            resolve(body.token);
                        })
                            .catch(function (err) {
                            console.log(err);
                            reject(new Error('* ID: ' + _this.id + ' * <-- Error while checking the licence: ' + err.response.body.errors));
                        });
                    })];
            });
        });
    };
    return MSClient;
}());
module.exports = MSClient;
