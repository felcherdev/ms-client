var request = require('request-promise');
module.exports = function () {
    var _this = this;
    return {
        read: function (id, searchByToken) {
            if (id === void 0) { id = null; }
            if (searchByToken === void 0) { searchByToken = false; }
            return request({
                url: _this.domain + '/auth/users' + (id ? '/' + id : ''),
                method: 'POST',
                headers: _this.getHeaders(),
                json: true,
                qs: {
                    searchByToken: searchByToken
                }
            });
        },
        create: function (data) {
            return request({
                url: _this.domain + '/auth/users/new',
                method: 'POST',
                headers: _this.getHeaders(),
                body: data,
                json: true
            });
        },
        update: function (id, data) {
            return request({
                url: _this.domain + '/auth/users/' + id,
                method: 'PATCH',
                headers: _this.getHeaders(),
                body: data,
                json: true
            });
        },
        delete: function (id, deleteTransaction // If passed as 'true', the 'id' field will be the transactionId to delete from the users table
        ) {
            if (deleteTransaction === void 0) { deleteTransaction = false; }
            return request({
                url: _this.domain + '/auth/users/' + id,
                method: 'DELETE',
                headers: _this.getHeaders(),
                json: true,
                qs: {
                    deleteTransaction: deleteTransaction
                }
            });
        }
    };
};
