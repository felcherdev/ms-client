var request = require('request-promise');
module.exports = function () {
    var _this = this;
    return {
        login: function (data) {
            return request({
                url: _this.domain + '/auth/public/login',
                method: 'POST',
                headers: _this.getHeaders(),
                body: data,
                json: true
            });
        },
        resetPassword: function (email) {
            return request({
                url: _this.domain + '/auth/public/password-reset/' + email,
                method: 'GET',
                headers: _this.getHeaders(),
                json: true
            });
        },
        confirmResetPassword: function (token, password) {
            return request({
                url: _this.domain + '/auth/public/password-reset/' + token,
                method: 'POST',
                headers: _this.getHeaders(),
                json: true,
                body: {
                    password: password
                }
            });
        }
    };
};
